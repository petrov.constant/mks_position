import requests
import logging
from datetime import datetime
from airflow.models import Variable
from airflow import DAG

from airflow.decorators import dag, task
from airflow.hooks.base import BaseHook
from airflow.providers.postgres.hooks.postgres import PostgresHook

API_CONN_ID='MKS'
POSTGRES_CONN_ID='pg-local'

@dag(schedule=Variable.get("mks_crontab"),, start_date=datetime(2023, 3, 5), catchup=False, max_active_runs=1)
def mks_flow():
    @task(task_id="mks_task", multiple_outputs=False)
    def get_mks_position():
        hook = BaseHook(None)
        conf = hook.get_connection(API_CONN_ID)
        data = requests.get(conf.host)
        logging.info(data.json())
        data=data.json()
        timestamp=int(data['timestamp'])
        message=data['message']
        latitude=float(data['iss_position']['latitude'])
        longitude=float(data['iss_position']['longitude'])

        pg_hook = PostgresHook.get_hook(POSTGRES_CONN_ID)
        sql = f'''INSERT INTO mks VALUES({timestamp},'{message}',{latitude},{longitude} )'''
        pg_hook.run(sql)

        
        

    get_mks_position()

mks_flow()